import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { DataService } from './data.service';

@Injectable()
export class PostService extends DataService {
  constructor(http: Http) {
    super('http://dev.nostratech.com:10093/api/v1/person', http)
    //url http://dev.nostratech.com:10093/api/v1/person merupakan base url untuk call api sample ini
  }
}

import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { AppError } from '../common/app-error';
import { BadInput } from '../common/bad-input';
import { NotFoundError } from '../common/not-found-error';

@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts: any[];
  posts2: any[];

  constructor(private service: PostService) {
  }

  ngOnInit() {
    this.getAll()
    // ini inisialisasi ketika load akan memanggil function getAll()
  }
  getAll(){
    //untuk dapetin smua contact
    this.service.getContact()
      .subscribe(posts => this.posts = posts.result);
      //this.posts ngerefer ke line 13 file ini yang isinya akan di isi dari response service
  }
  createPost() {
    
    let inputan = {
      "name": "CreateTest"+getRandomInt(1,1000),
        "address": "Jl.Suatu Jalan",
        "phone": "08123456789",
        "email": "abc@email.com"
    }

    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
  }
    this.service.createContact(inputan)
    //line 37 merupakan cara call service yang dimana disini akan passing parameter
      .subscribe(newPost => {
        this.getAll();
        //maksud di line atas ini setelah ada response, akan call getAll function untuk mereload 
        //hasil dari edit/add/delete yang sudah dilakukan
      }, 
      (error: AppError) => {
        if (error instanceof BadInput){
          // this.form.setErrors(error.originalError);
        } else {
          throw error;
        }
      });
  }

  updatePost(post) {
    // post.title = input.value;
    this.service.updateContact(post)
      .subscribe(updatedPost => {
        this.getAll();
        //maksud di line atas ini setelah ada response, akan call getAll function untuk mereload 
        //hasil dari edit/add/delete yang sudah dilakukan
      });
  }

  deletePost(post) {

    let id = '7454cc4f-2bda-4371-8b31-7aa189045ad7';
    this.service.deleteContact(post)
      .subscribe(
        () => {

        this.getAll();
        //maksud di line atas ini setelah ada response, akan call getAll function untuk mereload 
        //hasil dari edit/add/delete yang sudah dilakukan
      }, 
      (error: AppError) => {
        if (error instanceof NotFoundError){
          alert('This post has already been deleted.');
        } else {
          alert('An unexpected error occurred.');
          console.log(error)
        }
      });
  }
}
